import copy
from MFVStore.Entities.Address import Address
from MFVStore.Entities.Customer import Customer
from MFVStore.Entities.ShoppingCart import ShoppingCart
from MFVStore.Entities.User import User


class UserController:

    display_attributes = [
        "id", "last_name", "first_name", "email", "fail_login_no",
        "address", "phone_number", "balance"
    ]

    def __init__(self):
        self.__current_user = None
        self.__selected_user = None
        
    @property
    def selected_user(self):
        return self.__selected_user
    
    @selected_user.setter
    def selected_user(self, selected_user):
        self.__selected_user = selected_user
        
    @property
    def current_user(self):
        return self.__current_user

    @current_user.setter
    def current_user(self, current_user):
        self.__current_user = current_user

    def login(self, user_list, user_credential):
        self.__current_user = self.get_valid_user(user_list, user_credential)

    def get_user_credential(self, ui):
        user_name = ui.get_user_name()
        user_password = ui.get_password()
        return (user_name, user_password)

    def get_valid_user(self, user_list, user_credential):
        # user_credential[0] for login_user_id, user_credential[1] for password
        login_user_id = user_credential[0]
        password = user_credential[1]
        for id in user_list:
            user = user_list[id]
            if user.user_id == login_user_id:
                if self.user_suspended(user):
                    raise AttributeError("Account suspended")
                if user.password == password:
                    return user
        raise AttributeError("Incorrect User Id or Password")

    def user_suspended(self, user):
        if user.fail_login_no < 5:
            return False
        else:
            return True

    @staticmethod
    def gen_new_id(a_dict):
        if a_dict is not None:
            return max(a_dict) + 1
        else:
            return 1

    def register(self, ui, id_generator):
        new_customer = self.get_customer_from_ui(ui, id_generator)
        self.__current_user = new_customer
        self.__selected_user = new_customer
        self.display_account_detail(ui)

    def unregister(self, repo, ui, user_list):
        self.__selected_user = self.__current_user
        if self.__selected_user is not None and isinstance(self.__selected_user, Customer):
            customer_id = self.__selected_user.id
            self.__selected_user = None
            self.__current_user = None
            del user_list[customer_id]
            # repo.delete_customer(customer_id)
        else:
            raise AttributeError("Error in unregistering")
        ui.display_msg("Successfully unregister, and logged out!\n")

    def display_customers(self, ui, user_list):
        customer_list = dict((k, v) for k, v in user_list.items() if isinstance(v, Customer))
        ui.display_table("Below is the list of customers", customer_list, self.display_attributes)

    def display_account_detail(self, ui):
        ui.display_table_row(self.__selected_user, self.display_attributes)

    def get_customer_from_ui(self, ui, id_generator):
        last_name = ui.get_last_name()
        first_name = ui.get_first_name()
        password = ui.get_password()
        email = ui.get_email()
        fail_login_no = 0

        street_no = ui.get_street_no()
        street_addr = ui.get_street()
        city = ui.get_city()
        country = ui.get_country()
        postcode = ui.get_postcode()
        address = Address(street_no, street_addr, city, country, postcode)

        phone_number = ui.get_phone_number()

        shopping_cart = ShoppingCart(dict())
        order_history = dict()
        balance = 100
        customer_id = id_generator.get_customer_next_id()

        return Customer(customer_id, last_name, first_name, password, fail_login_no, email,
                        address, shopping_cart, order_history, phone_number, balance)

    def edit_account_from_ui(self, ui):
        if self.__selected_user is not None and isinstance(self.__selected_user, User):
            editable_attributes = {
                "last_name": {"Description": "Last name", "Function": self.update_last_name, "Type": str},
                "first_name": {"Description": "First name", "Function": self.update_first_name, "Type": str},
                "password": {"Description": "Password", "Function": self.update_password, "Type": str},
                "email": {"Description": "Email", "Function": self.update_email, "Type": str},
                "address": {"Description": "Address", "Function": self.update_address, "Type": Address},
                "phone_number": {"Description": "Phone number", "Function": self.update_phone_number, "Type": str},

            }
            ui.display([dict({k: {"Description": v["Description"]}}) for k, v in editable_attributes.items()])

            option = ui.get_user_option()
            new_value_str = ui.get_new_value(editable_attributes[option].get("Description"))
            new_value = editable_attributes[option]["Type"](new_value_str)
            editable_attributes[option]["Function"](new_value)
            ui.display_msg(editable_attributes[option].get("Description") + " is updated!")
            self.display_account_detail(ui)
        else:
            raise AttributeError("No selected account")

    def remove_customer(self, repo, ui, user_list):
        if self.__selected_user is not None and isinstance(self.__selected_user, Customer):
            customer_id = self.__selected_user.id
            del user_list[customer_id]
            #repo.delete_customer(customer_id)
        else:
            raise AttributeError("Selected customer is not valid")
        ui.display_msg("Successfully deleted the customer")

    def recharge(self, ui):
        amount = ui.get_amount()
        self.__current_user.balance += amount

    def update_last_name(self, new_value):
        self.__selected_user.last_name = new_value

    def update_first_name(self, new_value):
        self.__selected_user.first_name = new_value

    def update_password(self, new_value):
        self.__selected_user.password = new_value

    def update_email(self, new_value):
        self.__selected_user.email = new_value

    def update_address(self, new_value):
        self.__selected_user.address = new_value

    def update_phone_number(self, new_value):
        self.__selected_user.phone_number = new_value
