from MFVStore.Entities.Product import Product


class Fruit(Product):

    def __init__(self, product_id, product_name, product_source, description, package_type, stock_quantity,
                 package_price, discount_percentage, shelf_life_day, creation_datetime, expiry):
        super().__init__(product_id, product_name, product_source, description, package_type, stock_quantity,
                         package_price, discount_percentage, shelf_life_day, creation_datetime, expiry)

    def __str__(self):
        return "Product ID: " + str(self.product_id) + "\nProduct Name: " + self.product_name + \
               "\nProduct Source: " + self.product_source + "\nDescription: " + self.description + \
               "\nPackage Type: " + self.package_type + "\nStock Quantity: " + str(self.stock_quantity) + \
               "\nPackage Price: " + str(self.package_price) + "\nDiscount Percentage: " + \
               str(self.discount_percentage) + "\nShelf Life: " + str(self.shelf_life_day) + \
               "\nCreation Date & Time: " + str(self.creation_datetime) + "\nExpiry: " + str(self.expiry)

