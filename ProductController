from datetime import timedelta
from datetime import datetime
from MFVStore.Entities.Vegetable import Vegetable
from MFVStore.Entities.Fruit import Fruit
from MFVStore.Entities.Product import Product


class ProductController():

    display_attributes = [
        "product_id", "product_name", "product_source", "description", "package_type",
        "stock_quantity", "package_price", "current_price", "discount_percentage", "expiry"
    ]

    product_type_option = {
        "fruit": {"Description": "Fruit"},
        "vegetable": {"Description": "Vegetable"}
    }

    package_type_option = {
        "weight": {"Description": "By weight (kg)"},
        "bag": {"Description": "By bag"},
        "bunch": {"Description": "By bunch"},
        "one whole": {"Description": "One whole product"},
        "whole or half": {"Description": "One whole product or half product"}
    }

    product_source_option = {
        "overseas": {"Description": "Overseas"},
        "local": {"Description": "Local"}
    }

    discount_start_day = 1
    discount_percentage = 0.5

    def __init__(self):
        self.__selected_product = None

    @property
    def selected_product(self):
        return self.__selected_product

    @selected_product.setter
    def selected_product(self, product):
        self.__selected_product = product

    def display_products(self, ui, product_list):
        ui.display_table("Below is the list of products", product_list, self.display_attributes)

    def get_selected_product(self, product_list, product_id):
        if product_id in product_list:
            self.__selected_product = product_list[product_id]

    def add_new_product_from_ui(self, repo, ui, product_list, id_generator):
        self.__selected_product = self.get_new_product_from_ui(ui, id_generator)
        # repo.add_product
        if self.__selected_product.product_id not in product_list:
            product_list[self.__selected_product.product_id] = self.__selected_product
        ui.display_msg("Successfully added new product!")
        self.display_product_detail(ui)

    def get_new_product_from_ui(self, ui, id_generator):
        # logic to add new product or stock
        product_type = ui.get_product_type(self.product_type_option)
        product_name = ui.get_product_name()
        package_type = ui.get_package_type(self.package_type_option)
        quantity = ui.get_package_quantity()
        price = ui.get_package_price()
        source = ui.get_product_source(self.product_source_option)
        shelf_life = ui.get_shelf_life_day()
        description = ui.get_description()
        initial_discount = 0
        creation_datetime = datetime.utcnow()
        expiry = creation_datetime.date() + timedelta(shelf_life)
        product_id = id_generator.get_product_next_id()

        if product_type == "fruit":
            product = Fruit(product_id, product_name, source, description, package_type,
                            quantity, price, initial_discount, shelf_life, creation_datetime, expiry)
        else:
            product = Vegetable(product_id, product_name, source, description, package_type,
                                quantity, price, initial_discount, shelf_life, creation_datetime, expiry)
        return product

    def edit_product_from_ui(self, ui):
        if self.__selected_product is not None and isinstance(self.__selected_product, Product):
            editable_attributes = {
                "product_name": {
                    "Description": "Product name",
                    "Function": self.update_product_name,
                    "Type": str
                },
                "product_source": {
                    "Description": "Product source",
                    "Function": self.update_product_source,
                    "Type": str
                },
                "description": {
                    "Description": "Description",
                    "Function": self.update_description,
                    "Type": str
                },
                "package_type": {
                    "Description": "Package type",
                    "Function": self.update_package_type,
                    "Type": str
                },
                "stock_quantity": {
                    "Description": "Stock quantity",
                    "Function": self.update_stock_quantity,
                    "Type": float
                },
                "package_price": {
                    "Description": "Package price",
                    "Function": self.update_package_price,
                    "Type": float
                },
                "discount_percentage": {
                    "Description": "Discount percentage",
                    "Function": self.update_discount_percentage,
                    "Type": float
                },
                "shelf_life_day": {
                    "Description": "Shelf life in day",
                    "Function": self.update_shelf_life_day,
                    "Type": int
                }
            }
            ui.display([dict({k: {"Description": v["Description"]}}) for k, v in editable_attributes.items()])

            option = ui.get_user_option()
            if option not in editable_attributes:
                raise AttributeError("Incorrect input option")

            new_value_str = ui.get_new_value(editable_attributes[option].get("Description"))
            new_value = editable_attributes[option]["Type"](new_value_str)
            editable_attributes[option]["Function"](new_value)
            ui.display_msg(editable_attributes[option].get("Description") + " is updated!")
            self.display_product_detail(ui)
        else:
            raise AttributeError("No selected product")

    def remove_product(self, repo, ui, product_list):
        if self.__selected_product is not None and isinstance(self.__selected_product, Product):
            del product_list[self.__selected_product.product_id]
            #repo.delete_product(product_id)
        else:
            raise AttributeError("Selected product is not valid")
        ui.display_msg("Successfully deleted the product")

    @staticmethod
    def gen_new_id(a_dict):

        if a_dict is not None:
            return max(a_dict) + 1
        else:
            return 1

    def display_product_detail(self, ui):
        ui.display_table_row(self.__selected_product, self.display_attributes)

    def update_product_name(self, new_value):
        self.__selected_product.product_name = new_value

    def update_product_source(self, new_value):
        self.__selected_product.product_source = new_value

    def update_description(self, new_value):
        self.__selected_product.description = new_value

    def update_package_type(self, new_value):
        self.__selected_product.package_type = new_value

    def update_stock_quantity(self, new_value):
        self.__selected_product.stock_quantity = new_value

    def update_package_price(self, new_value):
        self.__selected_product.package_price = new_value

    def update_discount_percentage(self, new_value):
        self.__selected_product.discount_percentage = new_value

    def update_shelf_life_day(self, new_value):
        self.__selected_product.shelf_life_day = new_value
        self.__selected_product.expiry = self.__selected_product.creation_datetime.date() + \
                                       timedelta(new_value)

        if self.__selected_product.expiry - datetime.utcnow().date() <= timedelta(self.discount_start_day):
            self.__selected_product.discount_percentage = self.discount_percentage
